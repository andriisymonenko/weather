export interface WeatherTypeMapping {
  weather: string,
  type: string
}
