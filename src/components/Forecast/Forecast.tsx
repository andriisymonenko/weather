import { useFetchForecast } from "./getForecast";
import { Day } from "./Day";
import { useContext, useEffect } from "react";
import { DayContext } from "../../contexts/DayContext";

export const Forecast = () => {
  const weatherData = useFetchForecast().data;
  const [_, setDayWeather] = useContext(DayContext);

  useEffect(() => {
    if (weatherData && weatherData.length > 0) {
      setDayWeather(weatherData[0]);
    }
  }, [weatherData]);

  return (
    <div className="forecast">
      {
        weatherData?.slice(0, 7)
          .map((wd, index) => <Day key={index} dayEntity={wd} />)
      }
    </div>
  );
};