export interface DayEntity {
  date: string,
  values?: {
    humidity: number,
    windSpeed: number,
    windDirection: number,
    temperature: number,
    cloudCover: number,
    weather: string,
    weatherIconCode: number
  }
};