import { useContext } from "react";
import { DayContext } from "../../contexts/DayContext";
import { weatherTypeMappingData } from "../../Models/Forecast/weatherTypeMappingData";

export const Head = () => {
  const [dayWeatherState] = useContext(DayContext);
  const date = new Date(dayWeatherState.date);
  const type = weatherTypeMappingData.find(wt => wt.weather == dayWeatherState.values?.weather)?.type ?? "";

  return (
    <div className="head">
      <div className={`icon ${type}`}></div>
      <div className="current-date">
        <p>{date.toLocaleDateString('ru-RU', { weekday: 'long' })}</p>
        <span>{date.toLocaleDateString('ru-RU', { month: 'long', day: 'numeric' })}</span>
      </div>
    </div>
  );
};