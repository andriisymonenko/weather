import { CurrentWeather, Filter, Forecast, Head } from './components';
import { weatherQueryClient } from './QueryClients/weatherQueryClient';
import { QueryClientProvider } from 'react-query';
import { DayProvider } from './contexts/DayContext';

export const App = () => {
  return (
    <DayProvider>
      <QueryClientProvider client={weatherQueryClient}>
        <main>
          <Filter />
          <Head />
          <CurrentWeather />
          <Forecast />
        </main>
        {/* <ReactQueryDevtools initialIsOpen={false} /> */}
      </QueryClientProvider>
    </DayProvider>
  );
};
