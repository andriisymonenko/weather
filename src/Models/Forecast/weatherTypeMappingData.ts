import { WeatherTypeMapping } from "./WeatherTypeMapping";

export const weatherTypeMappingData: WeatherTypeMapping[] = [
  {
    weather: 'Mostly Cloudy',
    type: 'cloudy'
  },
  {
    weather: 'Partly Cloudy',
    type: 'cloudy'
  },
  {
    weather: 'Drizzle',
    type: 'rainy'
  },
  {
    weather: 'Flurries',
    type: 'rainy'
  },
  {
    weather: 'Light Rain',
    type: 'rainy'
  },
  {
    weather: 'Clear',
    type: 'sunny'
  }
];