import { useContext } from "react";
import { DayContext } from "../../contexts/DayContext";
import { weatherTypeMappingData } from "../../Models/Forecast/weatherTypeMappingData";

export const CurrentWeather = () => {
  const [selectedDay] = useContext(DayContext);
  const { temperature, weather, cloudCover, humidity } = selectedDay.values || {};
  const type = weatherTypeMappingData.find(wt => wt.weather == weather)?.type ?? "";

  return (
    <div className="current-weather">
      <p className="temperature">{Math.round(temperature || 0)}</p>
      <p className="meta">
        <span className={type}>{cloudCover}</span>
        <span className="humidity">{humidity}</span>
      </p>
    </div>
  );
};