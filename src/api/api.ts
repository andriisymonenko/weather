import { DayEntity } from "../Models/Forecast/DayEntity";

//https://lab.lectrum.io/rtx/api/v2/
const WEATHER_API_URL = process.env.REACT_APP_WEATHER_API_URL;

export const api = {
  async getWeather(): Promise<DayEntity[]> {
    const response = await fetch(`${WEATHER_API_URL}?city=Kiev`, {
      method: 'GET'
    });
    const result = await response.json();
    return result.data;
  }
};