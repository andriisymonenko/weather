import { useContext } from "react";
import { DayContext } from "../../contexts/DayContext";
import { DayEntity } from "../../Models/Forecast/DayEntity";
import { weatherTypeMappingData } from "../../Models/Forecast/weatherTypeMappingData";

export const Day: React.FC<DayProps> = props => {
  const [_, setDayWeather] = useContext(DayContext);

  const date = props.dayEntity.date;
  const { temperature, weather } = props.dayEntity.values || {};
  const type = weatherTypeMappingData.find(wt => wt.weather == weather)?.type ?? "";
  return (
    <div className={`day ${type}`} onClick={() => setDayWeather(props.dayEntity)}>
      <p>{new Date(date).toLocaleDateString('ru-RU', { weekday: 'long' })}</p>
      <span>{Math.round(temperature || 0)}</span>
    </div>
  );
};

interface DayProps {
  dayEntity: DayEntity
}