import { useQuery } from "react-query";
import { api } from "../../api";
import { DayEntity } from "../../Models/Forecast/DayEntity";

// export const getForecast = () => {
//   const [weatherData, setWeatherData] = useState<DayEntity[]>([]);
//   useEffect(() => {
//     (async () => {
//       const response = await api.getWeather('Kiev');
//       setWeatherData(response.weatherData ?? []);
//     })();
//   }, []);

//   return [weatherData];
// };

export const useFetchForecast = () => {
  const query = useQuery<DayEntity[]>('weather', api.getWeather)

  return query;
};