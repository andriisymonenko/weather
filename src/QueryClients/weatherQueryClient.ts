import { QueryClient } from 'react-query';

export const weatherQueryClient = new QueryClient();