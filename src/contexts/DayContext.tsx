import React, { createContext, SetStateAction, useState } from "react";
import { DayEntity } from "../Models/Forecast/DayEntity";

export const DayContext = createContext<DayProviderShape>([
  { date: '' },
  () => null
]);

export const DayProvider: React.FC = props => {
  const state = useState({ date: '' });

  return <DayContext.Provider value={state}>{props.children}</DayContext.Provider>;
}

type DayProviderShape = [DayEntity, React.Dispatch<SetStateAction<DayEntity>>];
